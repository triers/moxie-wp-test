<?php
/*
Plugin Name: Moxie Movie
Plugin URI:
Description: Displays movies of your choice on your WordPress Home page
Version: 0.1
Author: Orlin Dimitrov
Author URI:
Textdomain: moxie-movie
License: GNU General Public License v2
License URI: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*/

// Blocking direct access

defined('ABSPATH') or die("Page access not allowed!");

// Check WordPress version

global $wp_version;

$exit_message = __('Moxie Movie requires WordPress version 3.6 or newer. <a href=\"http://codex.wordpress.org/Upgrading_WordPress\">Please Update!</a>', 'moxie-movie');

if ( version_compare( $wp_version, "3.6", "<" ) ) {
    exit ( $exit_message );
}

if (! class_exists('MoxieMovie')) {

    class MoxieMovie {

        /**
         * Plugin class constructor
         */
        public function __construct() {

            // Initialize custom post type
            add_action( 'init', array($this, 'addCustomPostType') );

            // Create and update metaboxes
            add_action( 'add_meta_boxes', array( $this, 'addMetaBoxes' ) );
            add_action( 'save_post', array( $this, 'saveMetaBoxes' ) );

            // Add custom columns
            add_filter( 'manage_edit-movie_columns', array( $this, 'addCustomPostTypeCulumns') ) ;
            add_action( 'manage_movie_posts_custom_column', array( $this, 'addCustomPostTypeCulumnsData'), 10, 2 );
            add_action( 'manage_movie_posts_custom_column', array( $this, 'addCustomPostTypeCulumnsData'), 10, 2 );

            // Save JSON API Cache in DB on post save
            add_action( 'save_post', array( $this, 'createJSONAPICache'), 10, 3 );

            // Add Ajax Action for logged users
            add_action( 'wp_ajax_movie_json_api', array( $this, 'getJSONAPI') ) ;

            // Add Ajax Action for non-logged users
            add_action('wp_ajax_nopriv_movie_json_api', array( $this, 'getJSONAPI') ) ;

            $this->displayMoviesOnFrontpage();

        }

        /**
         * Adds custom post type 'Movie'
         */
        public function addCustomPostType() {

            register_post_type(
                'movie',
                array(
                    'labels' => array(
                        'name' => __('Movies', 'moxie-movie'),
                        'singular_name' => __('Movie', 'moxie-movie'),
                        'add_new' => __('Add New', 'moxie-movie'),
                        'add_new_item' => __('Add New Movie', 'moxie-movie'),
                        'edit' => __('Edit', 'moxie-movie'),
                        'edit_item' => __('Edit Movie', 'moxie-movie'),
                        'new_item' => __('New Movie', 'moxie-movie'),
                        'view' => __('View', 'moxie-movie'),
                        'view_item' => __('View Movie', 'moxie-movie'),
                        'search_items' => __('Search Movies', 'moxie-movie'),
                        'not_found' => __('No Movies Found', 'moxie-movie'),
                        'not_found_in_trash' => __('No Movies Found in Trash', 'moxie-movie'),
                        'parent' => __('Parent Movie', 'moxie-movie'),
                    ),
                    'rewrite' => array(
                        'slug' => 'movie',
                    ),
                    'supports' => array(
                        'title',
                        'editor',
                        'thumbnail',
                    ),
                    'query_var' => true,
                    'public' => true,
                    'publicly_queryable' => true,
                    'menu_position' => 4,
                    'has_archive' => true,
                )
            );

        }

        /**
         * Adds custom columns to Movie post type
         */
        public function addCustomPostTypeCulumns( $columns ) {

            $columns = array(
                'cb' => '<input type="checkbox" />',
                'title' => __( 'Movie', 'moxie-movie'),
                'year' => __( 'Year', 'moxie-movie'),
                'rating' => __( 'Rating', 'moxie-movie'),
                'date' => __( 'Date', 'moxie-movie'),
            );

            return $columns;
        }

        /**
         * Adds custom columns to Movie post type
         */
        public function addCustomPostTypeCulumnsData( $column, $post_id ) {

            global $post;

            switch( $column ) {

                case 'year' :

                    $movie_year = get_post_meta( $post_id, '_movie_year', true );

                    if ( empty( $movie_year ) ) {
                        echo __( 'N/A', 'moxie-movie' );
                    } else {
                        echo $movie_year;
                    }

                    break;

                case 'rating' :

                    $movie_rating = get_post_meta( $post_id, '_movie_rating', true );

                    if ( empty( $movie_rating ) ) {

                        $movie_rating = "default";
                        echo "<img src='".plugin_dir_url( __FILE__ )."images/rating-".$movie_rating.".png' style='width: 100%; margin-top: 5px;' />";

                    } else {

                        echo "<img src='".plugin_dir_url( __FILE__ )."images/rating-".$movie_rating.".png' style='width: 100%; margin-top: 5px;' />";

                    }

                    break;

                default :
                    break;

            }
        }

        /**
         * Adds the meta box container.
         */
        public function addMetaBoxes( $post_type ) {

            $post_types = array('movie');     // limit meta box to movie post type

            if ( in_array( $post_type, $post_types )) {
                add_meta_box(
                    'some_meta_box_name'
                    ,__( 'Movie Details', 'moxie-movie' )
                    ,array( $this, 'render_meta_box_content' )
                    ,$post_type
                    ,'advanced'
                    ,'high'
                );
            }

        }

        /**
         * Save the meta when the post is saved.
         */
        public function saveMetaBoxes( $post_id ) {

            // Check if our nonce is set.
            if ( ! isset( $_POST['myplugin_inner_custom_box_nonce'] ) )
                return $post_id;

            $nonce = $_POST['myplugin_inner_custom_box_nonce'];

            // Verify that the nonce is valid.
            if ( ! wp_verify_nonce( $nonce, 'myplugin_inner_custom_box' ) )
                return $post_id;

            // If this is an autosave, our form has not been submitted,
            //     so we don't want to do anything.
            if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
                return $post_id;

            // Check the user's permissions.
            if ( 'page' == $_POST['post_type'] ) {

                if ( ! current_user_can( 'edit_page', $post_id ) )
                    return $post_id;

            } else {

                if ( ! current_user_can( 'edit_post', $post_id ) )
                    return $post_id;
            }

            /* OK, its safe for us to save the data now. */

            // Sanitize the user input.
            $movie_year = sanitize_text_field( $_POST['movie_year'] );

            // Update the meta field.
            update_post_meta( $post_id, '_movie_year', $movie_year );

            // Sanitize the user input.
            $movie_rating = sanitize_text_field( $_POST['movie_rating'] );

            // Update the meta field.
            update_post_meta( $post_id, '_movie_rating', $movie_rating );
        }


        /**
         * Render Meta Box content.
         */
        public function render_meta_box_content( $post ) {

            // Add an nonce field so we can check for it later.
            wp_nonce_field( 'myplugin_inner_custom_box', 'myplugin_inner_custom_box_nonce' );

            // Use get_post_meta to retrieve an existing value from the database.
            $movie_year = get_post_meta( $post->ID, '_movie_year', true );
            $movie_rating = get_post_meta( $post->ID, '_movie_rating', true );

            // Display the form, using the current value.

            echo '<label for="movie_year">';
            _e( 'Year:', 'moxie-movie' );
            echo '</label> ';
            echo '<input type="text" id="movie_year" name="movie_year"';
            echo ' value="' . esc_attr( $movie_year ) . '" size="5" />';

            echo "&nbsp;&nbsp;&nbsp;";

            // Display the form, using the current value.

            echo '<label for="movie_rating">';
            _e( 'Rating:', 'moxie-movie' );
            echo '</label> ';

            echo '<input type="radio" name="movie_rating" value="1" '; if($movie_rating == "1") echo ' checked '; echo '>1 &nbsp;';
            echo '<input type="radio" name="movie_rating" value="2" '; if($movie_rating == "2") echo ' checked '; echo '>2 &nbsp;';
            echo '<input type="radio" name="movie_rating" value="3" '; if($movie_rating == "3") echo ' checked '; echo '>3 &nbsp;';
            echo '<input type="radio" name="movie_rating" value="4" '; if($movie_rating == "4") echo ' checked '; echo '>4 &nbsp;';
            echo '<input type="radio" name="movie_rating" value="5" '; if($movie_rating == "5") echo ' checked '; echo '>5 &nbsp;';
        }

        /**
         * Create JSON API Cache
         */
        public function createJSONAPICache() {

            $posttype = 'movie';

            $json_data = array();

            // If this isn't a 'movie' post, don't update it.

            if ( $slug != $post->post_type ) {

                return;

            } else {

                    $MovieData = get_posts(array('post_type' => $posttype));

                    if (! empty($MovieData)) {

                        foreach ($MovieData as $MovieDataItem) {

                            $movie_year = get_post_meta( $MovieDataItem->ID, '_movie_year', true );
                            $movie_rating = get_post_meta( $MovieDataItem->ID, '_movie_rating', true );

                            $movie_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $MovieDataItem->ID ), 'single-post-thumbnail' );

                            if($movie_thumbnail[0] == "") {
                                $movie_image = plugin_dir_url( __FILE__ )."images/no_poster.jpg";
                            } else {
                                $movie_image = $movie_thumbnail[0];
                            }

                            $json_data['data'][] = array(
                                'id' => (int) $MovieDataItem->ID,
                                'title' => $MovieDataItem->post_title,
                                'poster_url' => $movie_image,
                                'rating' => (int) $movie_rating,
                                'year' => (int) $movie_year,
                                'short_description' => $MovieDataItem->post_content
                            );
                        }

                        // Save JSON data as option in database

                        $option_name = 'Movie_JSON_Data' ;

                        if ( get_option( $option_name ) !== false ) {

                            // The option already exists, so we just update it.
                            update_option( $option_name, $json_data );

                        } else {

                            // The option hasn't been added yet. We'll add it with $autoload set to 'no'.
                            $deprecated = null;
                            $autoload = 'no';
                            add_option( $option_name, $json_data, $deprecated, $autoload );
                        }

                    }

                    }
        }

        /**
         * Get JSON API Data cached from the database
         */
        public function getJSONAPI() {

            // Load cached data directly from the database
            wp_send_json(get_option( 'Movie_JSON_Data' ));
            wp_die();
        }

        /**
         * Show movies catalogue on the front page
         */
        public function displayMoviesOnFrontpage()
        {

            add_filter(
                'the_content',
                function () {
                    if (is_home() || is_front_page()) {
                        $output = '<style>
                        /* Hide default elements */
                        .entry-header,
                        .entry-meta {
                            display: none;
                        }

                        /* Custom Styles */

                        #moxie-movies-loading,
                        #moxie-movies-empty {
                            padding: 10px;
                            color: green;
                        }

                        #moxie-movies-empty {
                            display: none;
                        }

                        #moxie-movies-empty-text {
                            position: absolute;
                            top: 45%;
                            left: 20%;
                            z-index: 200;
                            color: yellow;
                            font-weight: bold;
                            background: #000;
                            padding: 5px;
                        }

                        .movie-wrapper {
                            padding: 10px;
                            width: 50%;
                            float: left;
                            display: none;
                        }

                        .movie {
                            padding: 10px;
                            background-color: #333;
                            color: #fff;
                            -webkit-border-radius: 10px;
                            -moz-border-radius: 10px;
                            border-radius: 10px;
                        }

                        .movie a {
                            color: #ffcf00;
                        }

                        .movie a:hover {
                            text-decoration: underline;
                        }

                        .movie-image {
                            max-width: 100%;
                            display: block;
                        }


                        h3.movie-heading {
                            font-size: 24px;
                            margin-bottom: 10px;
                            margin-top: 10px;
                        }

                        h3.movie-heading small {
                            font-size: 20px;
                        }

                        .movie-rating {
                            margin-bottom: 10px;
                        }

                        .movie-description {
                            font-size: 14px;
                        }


                        @media only screen and (max-width : 520px) {

                             .movie-wrapper {
                                width: 100%;
                            }

                        }

                    </style>
                    <script>
                        jQuery(document).ready(function() {
                            jQuery.ajax({
                                url: "'.get_site_url().'/wp-admin/admin-ajax.php",
                                data: {
                                    action: "movie_json_api",
                                },
                                success: function(response) {
                                    var data = response.data;

                                    // Force a little delay, so we can view the loading info.
                                    setTimeout(function() {
                                        jQuery("#moxie-movies-loading")
                                                  .delay(1000)
                                                  .fadeOut(2000);

                                        if (data) {

                                            var movieTemplate = "<div id=\"movie-wrapper-{id}\" class=\"movie-wrapper\"><div id=\"movie-{id}\" class=\"movie\">"+
                                                "<img class=\"movie-image\" src=\"{poster_url}\" />"+
                                                "<h3 class=\"movie-heading\">"+
                                                    "{title} "+
                                                    "<small>({year})</small>"+
                                                "</h3>"+
                                                "<div class=\"movie-rating\">"+
                                                    "<img src=\"'.plugin_dir_url( __FILE__ ).'images/rating-{rating}.png\" style=\"\" />"+
                                                "</div>"+
                                                "<div class=\"movie-description\">{short_description}</div>"+
                                            "</div></div>";

                                            for (var i = 0; i < data.length; i++) {
                                                var thisMovie = data[i];

                                                if(thisMovie.rating == "") {
                                                    thisMovie.rating = "default";
                                                }

                                                if(thisMovie.year == 0) {
                                                    thisMovie.year = "N/A";
                                                }

                                                var thisMovieTemplate = movieTemplate
                                                    .replace("{id}", thisMovie.id)
                                                    .replace("{title}", thisMovie.title)
                                                    .replace("{poster_url}", thisMovie.poster_url)
                                                    .replace("{rating}", thisMovie.rating)
                                                    .replace("{year}", thisMovie.year)
                                                    .replace("{short_description}", thisMovie.short_description)
                                                ;

                                                jQuery(thisMovieTemplate)
                                                    .delay(3000)
                                                    .appendTo("#moxie-movies")
                                                    .delay(i*500)
                                                    .fadeIn(2000)
                                                ;
                                            }
                                        } else {
                                            jQuery("#moxie-movies-empty")
                                            .delay(3000)
                                            .fadeIn(2000);
                                        }
                                    }, 1000);
                                },
                            });
                        });
                    </script>
                    <div id="moxie-movies-wrapper">
                        <h1>'.__( 'Movie Catalogue', 'moxie-movie' ).'</h1>
                        <div id="moxie-movies-loading"><img src="'.plugin_dir_url( __FILE__ ).'/images/loading_movie.gif" style="" /></div>
                        <div id="moxie-movies-empty">
                            <img src="'.plugin_dir_url( __FILE__ ).'/images/tv.gif" style="" />
                            <div id="moxie-movies-empty-text">'.__( 'No Movies Found!', 'moxie-movie' ).'</div>
                        </div>
                        <div id="moxie-movies"></div>
                    </div>';

                        echo $output;
                    }
                }
            );
        }

    }

// Initialize plugin
$moxieMovies = new MoxieMovie();

}
